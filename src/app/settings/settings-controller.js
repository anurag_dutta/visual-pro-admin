'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('settingsCtrl', function ($scope,$mdSidenav,localDataService, WorkType, $mdToast, $mdDialog, LeaveType, Holiday) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.data = {workedit: null, leaveedit:null};
    $scope.localData = localDataService;

    $scope.editWorkType = function(index, type) {
    	$scope.data.newWorkType = type.type;
    	$scope.data.workedit = index;
    };
    $scope.cancelWorkEdit = function() {
    	$scope.data.newWorkType = null;
    	$scope.data.workedit = null;
    };

    $scope.moveWorkDown = function(type) {
        if(type.seq==$scope.localData.workTypes.length) {
            return;
        }
        var nextIndex,currentIndex;
        for(var i=0;i<$scope.localData.workTypes.length;i++) {
            var item = $scope.localData.workTypes[i];
            if(item.seq==type.seq) {
                currentIndex = i;
            }
            if(item.seq==(type.seq+1)) {
                nextIndex = i;
            }
        }
        
        WorkType.updateAll({"where":{"id":$scope.localData.workTypes[nextIndex].id}},{"seq":type.seq},function(success){
            WorkType.updateAll({"where":{"id":$scope.localData.workTypes[currentIndex].id}},{"seq":(type.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });

        $scope.localData.workTypes[nextIndex].seq = type.seq;
        $scope.localData.workTypes[currentIndex].seq = type.seq+1;
    };

    $scope.moveWorkUp = function(type) {
        if(type.seq==1) {
            return;
        }
        var previousIndex,currentIndex;
        for(var i=0;i<$scope.localData.workTypes.length;i++) {
            var item = $scope.localData.workTypes[i];
            if(item.seq==type.seq) {
                currentIndex = i;
            }
            if(item.seq==(type.seq-1)) {
                previousIndex = i;
            }
        }
        
        WorkType.updateAll({"where":{"id":$scope.localData.workTypes[previousIndex].id}},{"seq":type.seq},function(success){
            WorkType.updateAll({"where":{"id":$scope.localData.workTypes[currentIndex].id}},{"seq":(type.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });
        $scope.localData.workTypes[previousIndex].seq = type.seq;
        $scope.localData.workTypes[currentIndex].seq = type.seq-1;
        
    };

    $scope.moveLeaveUp = function(type) {
        if(type.seq==1) {
            return;
        }
        var previousIndex,currentIndex;
        for(var i=0;i<$scope.localData.leaveTypes.length;i++) {
            var item = $scope.localData.leaveTypes[i];
            if(item.seq==type.seq) {
                currentIndex = i;
            }
            if(item.seq==(type.seq-1)) {
                previousIndex = i;
            }
        }
        
        LeaveType.updateAll({"where":{"id":$scope.localData.leaveTypes[previousIndex].id}},{"seq":type.seq},function(success){
            LeaveType.updateAll({"where":{"id":$scope.localData.leaveTypes[currentIndex].id}},{"seq":(type.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });
        $scope.localData.leaveTypes[previousIndex].seq = type.seq;
        $scope.localData.leaveTypes[currentIndex].seq = type.seq-1;
        
    };
    $scope.moveLeaveDown = function(type) {
        if(type.seq==$scope.localData.workTypes.length) {
            return;
        }
        var nextIndex,currentIndex;
        for(var i=0;i<$scope.localData.workTypes.length;i++) {
            var item = $scope.localData.workTypes[i];
            if(item.seq==type.seq) {
                currentIndex = i;
            }
            if(item.seq==(type.seq+1)) {
                nextIndex = i;
            }
        }
        
        WorkType.updateAll({"where":{"id":$scope.localData.workTypes[nextIndex].id}},{"seq":type.seq},function(success){
            WorkType.updateAll({"where":{"id":$scope.localData.workTypes[currentIndex].id}},{"seq":(type.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });

        $scope.localData.leaveTypes[nextIndex].seq = type.seq;
        $scope.localData.leaveTypes[currentIndex].seq = type.seq+1;
    };

    $scope.saveLeaveEdit = function(type) {
    	LeaveType.updateAll({"where":{"id":type.id}},{"type":$scope.data.newLeaveType},function(success){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Work type updated')
    		    .hideDelay(3000)
    		);
    		$scope.data.newLeaveType = null;
    		$scope.data.leaveedit = null;
    		$scope.localData.bootData();
    	}, function(error){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Failed to update work type')
    		    .hideDelay(3000)
    		);
    		$scope.data.newLeaveType = null;
    		$scope.data.leaveedit = null;
    	});
    };

    $scope.createWorkType = function(type) {
    	WorkType.create({"type":type},function(success){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Work type added')
    		    .hideDelay(3000)
    		);
    		$scope.newWorkType = null;
    		$scope.localData.bootData();
    	}, function(error){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Failed to add work type')
    		    .hideDelay(3000)
    		);
    	});
    };

    $scope.deleteWorkType = function(type) {
    	var confirm = $mdDialog.confirm()
    	      .title('Are you sure you want to delete this work type?')
    	      .ok('Yes')
    	      .cancel('No');
    	    $mdDialog.show(confirm).then(function() {
    	      WorkType.deleteById({"id":type.id},function(success){
    	      	$mdToast.show(
    	      	  $mdToast.simple()
    	      	    .content('Work type deleted')
    	      	    .hideDelay(3000)
    	      	);
    	      	$scope.localData.bootData();
    	      }, function(error){
    	      	$mdToast.show(
    	      	  $mdToast.simple()
    	      	    .content('Could not delete work type')
    	      	    .hideDelay(3000)
    	      	);
    	      });
    	    }, function() {
    	      
    	    });
    };

    $scope.editLeaveType = function(index, type) {
    	$scope.data.newLeaveType = type.type;
    	$scope.data.leaveedit = index;
    };
    $scope.cancelLeaveEdit = function() {
    	$scope.data.newLeaveType = null;
    	$scope.data.leaveedit = null;
    };

    $scope.saveLeaveEdit = function(type) {
    	LeaveType.updateAll({"where":{"id":type.id}},{"type":$scope.data.newLeaveType},function(success){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Leave type updated')
    		    .hideDelay(3000)
    		);
    		$scope.data.newLeaveType = null;
    		$scope.data.leaveedit = null;
    		$scope.localData.bootData();
    	}, function(error){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Failed to update leave type')
    		    .hideDelay(3000)
    		);
    		$scope.data.newLeaveType = null;
    		$scope.data.leaveedit = null;
    	});
    };

    $scope.createLeaveType = function(type) {
    	LeaveType.create({"type":type},function(success){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Leave type added')
    		    .hideDelay(3000)
    		);
    		$scope.newLeaveType = null;
    		$scope.localData.bootData();
    	}, function(error){
    		$mdToast.show(
    		  $mdToast.simple()
    		    .content('Failed to add leave type')
    		    .hideDelay(3000)
    		);
    	});
    };

    function showToast(message) {
    	$mdToast.show(
    	  $mdToast.simple()
    	    .content(message)
    	    .hideDelay(3000)
    	);
    }

    $scope.deleteLeaveType = function(type) {
    	var confirm = $mdDialog.confirm()
    	      .title('Are you sure you want to delete this leave type?')
    	      .ok('Yes')
    	      .cancel('No');
    	    $mdDialog.show(confirm).then(function() {
    	      LeaveType.deleteById({"id":type.id},function(success){
    	      	$mdToast.show(
    	      	  $mdToast.simple()
    	      	    .content('Leave type deleted')
    	      	    .hideDelay(3000)
    	      	);
    	      	$scope.localData.bootData();
    	      }, function(error){
    	      	$mdToast.show(
    	      	  $mdToast.simple()
    	      	    .content('Could not delete leave type')
    	      	    .hideDelay(3000)
    	      	);
    	      });
    	    }, function() {
    	      
    	    });
    };

    $scope.createHoliday = function(date,description) {
    	date = moment(date).startOf('day').toDate();
    	Holiday.create({"date":date,"description":description},function(success){
    		showToast("Holiday added successfully");
    		$scope.newHoliday = null;
    		$scope.newHolidayDate = null;
    		$scope.localData.bootData();
    	}, function(error){
    		showToast("Failed to add holiday. Please try again!");
    	});
    };

    $scope.saveHolidayEdit = function(holiday) {
    	$scope.data.newHolidayDate = moment($scope.data.newHolidayDate).startOf('day').toDate();
    	Holiday.updateAll({"where":{"id":holiday.id}},{"date":$scope.data.newHolidayDate,"description":$scope.data.newHoliday},function(success){
    		showToast("Holiday updated successfully");
    		$scope.data.newHoliday = null;
    		$scope.data.newHolidayDate = null;
    		$scope.data.holidayedit = null;
    		$scope.localData.bootData();
    	}, function(error){
    		showToast("Holiday update failed");
    	});
    };

    $scope.editHolidayType = function(index,holiday) {
    	$scope.data.holidayedit = index;
    	$scope.data.newHoliday = holiday.description;
    	$scope.data.newHolidayDate = moment(holiday.date,moment.ISO_8601).toDate();
    };

    $scope.cancelHolidayEdit = function() {
    	$scope.data.newHoliday = null;
    	$scope.data.newHolidayDate = null;
    	$scope.data.holidayedit = null;
    };

    $scope.deleteHolidayType = function(holiday) {
    	Holiday.deleteById({"id":holiday.id},function(success){
    		showToast("Holiday deleted successfully");
    		$scope.localData.bootData();
    	}, function(error){
    		showToast("Holiday delete failed");
    	});
    };
});
