'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('resourceCtrl', function ($scope,$mdSidenav,localDataService,$mdDialog, localUserFactory, $state, User, Shift, Leave,$mdToast) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.localData = localDataService;
    $scope.newResource = {
     
    };
    $scope.shifts = [];
    $scope.newShift = {};
    $scope.data = {step:0,mode:'Add',showblank: true,month:moment().get('month')+1};
    
    $scope.createResource = function(newResource,manualPassword,confirmPassword) {
      if($scope.data.mode=="Edit") {
        var updatedResource = _.omit(newResource,'id');
        User.updateAll({"where":{id:$scope.newResource.id}},updatedResource,function(result){
          $scope.alert = $mdDialog.alert({
            title: 'Success',
            content: 'Updated user details',
            ok: 'Ok'
          });
          $mdDialog
            .show( $scope.alert )
            .finally(function() {
              $scope.alert = undefined;
              $scope.data = {step:0,mode:'Add'};
              $scope.newResource = {};
              $scope.localData.bootData();
          });
        },function(err){
          console.log(err);
        });
      }
      else {
        if(manualPassword) {
          if(newResource.password !== confirmPassword) {
            $scope.alert = $mdDialog.alert({
              title: 'Attention',
              content: 'Passwords don\'t match!',
              ok: 'Close'
            });
            $mdDialog
              .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
            });
            return;
          }
        }
        newResource.roles = ["resource"];
        if(newResource.isAdmin) {
          newResource.roles.push('admin');
        }
        User.create(newResource,function(success){
          $scope.newResource = {};
          $scope.data = {step:0,mode:'Add'};
          $scope.localData.bootData();
        });
      }
    };
    $scope.doLogout = function() {
      console.log(localUserFactory.userData);
      User.logout(function(result){
        if(result.$resolved) {
         localUserFactory.authentication = null;
         $state.go('login');
        }
        else {
          console.log("Logout Failed");
        }
      });
    };

    $scope.batchEdit = function() {
      var changingShifts = [];
      _.each($scope.shifts,function(shift){
        if(shift.selected) {
          changingShifts.push(shift.id);
        }
      });
      $scope.data.editData = {
        changingShifts: changingShifts
      };
      $scope.data.addingShift = true;
      $scope.$broadcast('edit-mode');
    };
    $scope.batchDelete = function() {
      var changingShifts = [];
      _.each($scope.shifts,function(shift){
        if(shift.selected) {
          changingShifts.push(shift.id);
        }
      });
      Shift.removeShifts({"shifts":changingShifts},function(success){
        $scope.alert = $mdDialog.alert({
          title: 'Success',
          content: 'Shifts deleted successfully.',
          ok: 'Ok'
        });
        $mdDialog
          .show( $scope.alert )
          .finally(function() {
            $scope.alert = undefined;
            $scope.data.edit = undefined;
            _getExistingShifts($scope.localData.resources[$scope.data.selectedResource].id);
        });
      },function(error){

      });
    };

    $scope.toList = function(types) {
      var list = '';
      _.each(types,function(type,index){
        if(index==types.length-1) {
          list += type.type;
        }
        else {
          list += type.type+',';
        }
      });
      return list;
    };

    $scope.findResource = function(id) {
      var result = _.find($scope.localData.resources,function(item){return item.id==id});
      return result.firstName+' '+result.lastName;
    };

    $scope.edit = function(user) {
      $scope.newResource = user;
      var types = [];
      _.each($scope.newResource.types,function(orgType){
        if(orgType) {
          var item = _.find($scope.localData.workTypes,function(baseType){
            return baseType.type==orgType.type;
          });
          types.push(item);
        }
      });
      $scope.newResource.types = types;
      $scope.data.mode = 'Edit';
      $scope.data.step = 1;
    };
    $scope.delete = function(resource) {
      var confirm = $mdDialog.confirm()
            .title('Are you sure you want to delete this user?')
            .content('This is not a reversible process.')
            .ariaLabel('Delete Resource')
            .ok('Yes')
            .cancel('Cancel');
          $mdDialog.show(confirm).then(function() {
            User.deleteById({"id":resource.id},function(result){
              if(result.$resolved) {
                var message = "User deleted successfully";
              }
              else {
                var message = "Failed to delete user. Please try again";
              }
              $scope.localData.bootData();
              var alert = $mdDialog.alert({
                content: message,
                ok: 'Ok'
              });
              $mdDialog
                .show( alert )
                .finally(function() {
                  alert = undefined;
              });
            });
          }, function() {
            $scope.alert = 'You decided to keep your debt.';
          });
    };
    $scope.selectShift = function(shift) {
      console.log("Selecting shift");
      $scope.newShift = shift;
    };

    $scope.$on('close-form',function(){
      $scope.data.addingShift = false;
      $scope.data.editData = undefined;
      _getExistingShifts($scope.localData.resources[$scope.data.selectedResource].id);
    });

    function parseTime(input) {
      var result = input.split(':');
      result[0] = parseInt(result[0]);
      result[1] = result[1].split(' ');
      if(result[1][1]=='pm') {
        result[0] = result[0]+12;
      }
      result[1] = parseInt(result[1][0]);
      return result;
    }

    $scope.addShift = function() {
      // $scope.shifts.push($scope.newShift);
      // $scope.newShift = {};
      var start = moment($scope.newShift.from);
      var startTimeArray = $scope.newShift.startTime.split(':');
      startTimeArray[0] = parseInt(startTimeArray[0]);
      startTimeArray[1] = startTimeArray[1].split(' ');
      if(startTimeArray[1][1]=='PM') {
        startTimeArray[0] = startTimeArray[0]+12;
      }
      startTimeArray[1] = parseInt(startTimeArray[1][0]);

      var end = moment($scope.newShift.to);
      var endTimeArray = $scope.newShift.endTime.split(':');
      endTimeArray[0] = parseInt(endTimeArray[0]);
      endTimeArray[1] = endTimeArray[1].split(' ');
      if(endTimeArray[1][1]=='PM') {
        endTimeArray[0] = endTimeArray[0]+12;
      }
      endTimeArray[1] = parseInt(endTimeArray[1][0]);

      var break1Array = $scope.newShift.break1.split(':');
      break1Array[0] = parseInt(break1Array[0]);
      break1Array[1] = break1Array[1].split(' ');
      if(break1Array[1][1]=='PM') {
        break1Array[0] = break1Array[0]+12;
      }
      break1Array[1] = parseInt(break1Array[1][0]);

      var break2Array = $scope.newShift.break2.split(':');
      break2Array[0] = parseInt(break2Array[0]);
      break2Array[1] = break2Array[1].split(' ');
      if(break2Array[1][1]=='PM') {
        break2Array[0] = break2Array[0]+12;
      }
      break2Array[1] = parseInt(break2Array[1][0]);

      var item = {
        start: start.toISOString(),
        end: end.toISOString(),
        breaks: [break1Array,break2Array],
        startTime: startTimeArray,
        endTime: endTimeArray,
        period: $scope.newShift.period,
        resourceID: $scope.data.resource.id
      };
      Shift.create(item,function(result){
        if(result.$resolved) {
          $scope.alert = $mdDialog.alert({
            title: 'Success',
            content: 'Shift added successfully',
            ok: 'Ok'
          });
          $mdDialog
            .show( $scope.alert )
            .finally(function() {
              $scope.alert = undefined;
              $scope.getExistingShifts($scope.data.resource.id);
          });
        }
        else {
          $scope.alert = $mdDialog.alert({
            title: 'Failure',
            content: 'Failed to add shift',
            ok: 'Ok'
          });
          $mdDialog
            .show( $scope.alert )
            .finally(function() {
              $scope.alert = undefined;
          });
        }
      });
    };

    $scope.setOrderParam = function(param) {
      if($scope.orderParam==param) {
        $scope.orderParam = '-'+param;
      }
      else {
        $scope.orderParam = param;
      }
    };

    $scope.selectMonth = function(month) {
      $scope.data.month = month;
      if($scope.data.selectedResource!=undefined) {
        $scope.shifts = null;
        _getExistingShifts($scope.localData.resources[$scope.data.selectedResource].id);
      }
    };

    function _getExistingShifts(resourceID) {
      Shift.getShifts({"resourceID":resourceID,"month":$scope.data.month},function(result){
        _.each(result.shifts,function(shift){
          shift.holiday = _.find($scope.localData.holidays,function(item){
            return item.date==shift.start;
          });
        });
        $scope.shifts = result.shifts;
        console.log("Got shifts for "+resourceID,result);
      },function(error){
        console.log("Error getting shifts",error);
      });
      // Shift.find({"filter":{"where":{"resourceID":resourceID}}},function(result){
      //   $scope.shifts = result;
      // });
    }

    $scope.selectResource = function(index,resource) {
      $scope.data.selectedResource = index;
      $scope.data.showblank = false;
      $scope.shifts = null;
      _getExistingShifts(resource.id);
    };

    $scope.selectAll = function(value) {
      _.each($scope.shifts,function(shift){
        shift.selected = value;
      });
    };

    $scope.deleteDayShift = function(shift) {
      Shift.deleteById({"id":shift.id},function(success){
        $mdToast.show(
          $mdToast.simple()
            .content('Shift Deleted')
            .hideDelay(3000)
        );
        _getExistingShifts($scope.localData.resources[$scope.data.selectedResource].id);
      },function(error){
        $mdToast.show(
          $mdToast.simple()
            .content('Could not delete shitf. Please try again!')
            .hideDelay(3000)
        );
      })
    };

    $scope.saveDayShift = function(data,shift) {
      var item = {
        startTime: parseTime($scope.data.newStartTime),
        endTime: parseTime($scope.data.newEndTime),
        breaks: [parseTime($scope.data.newBreak1),parseTime($scope.data.newBreak2)]
      };
      Shift.updateAll({"where":{"id":shift.id}},item,function(success){
        $scope.alert = $mdDialog.alert({
          title: 'Success',
          content: 'Shift updated successfully.',
          ok: 'Ok'
        });
        $mdDialog
          .show( $scope.alert )
          .finally(function() {
            $scope.alert = undefined;
            $scope.data.edit = undefined;
            _getExistingShifts($scope.localData.resources[$scope.data.selectedResource].id);
        });
        console.log(success);
      },function(error){
        console.error(error);
      });
    };

    $scope.periodChange = function(period) {
      var today = moment().startOf(period).add(1,period);
      var from = today.toDate();
      $scope.newShift.from = from;
      console.log(today.toDate());
      var to = moment().startOf(period).add(2,period);
      $scope.newShift.to = to.toDate();
    };
    $scope.fromDateChanged = function(fromDate,period) {
      var to = moment(fromDate);
      to.add(1,period);
      $scope.newShift.to = to.toDate(); 
    };
    $scope.resourcesearch = function(query) {
      query = angular.lowercase(query);
      var result = _.filter($scope.localData.resources,function(resource){
        var test = angular.lowercase(resource.firstName)+angular.lowercase(resource.lastName);
        return (test.indexOf(query)===0);
      });
      return result;
    };

    $scope.editLeave = function(index,leave) {
      $scope.data.leaveedit=index;
      $scope.data.leavestart = moment(leave.start,moment.ISO_8601).toDate();
      $scope.data.leaveend = moment(leave.end,moment.ISO_8601).toDate();
      $scope.data.leavetype = leave.type;
    };

    $scope.deleteLeave = function(leave) {
      Leave.deleteById({"id":leave.id},function(success){
        $scope.localData.bootData();
        $mdToast.show(
          $mdToast.simple()
            .content('Leave deleted')
            .hideDelay(3000)
        );
      },function(error){
        $mdToast.show(
          $mdToast.simple()
            .content('Failed to delete leave')
            .hideDelay(3000)
        );
      })
    };

    $scope.saveLeave = function(leave) {
      Leave.deleteById({"id":leave.id},function(sucess){
        leave.start = moment($scope.data.leavestart).toISOString();
        leave.end = moment($scope.data.leaveend).toISOString();
        leave.type = $scope.data.leavetype;
        Leave.createLeave({"data":leave},function(success){
          $scope.localData.bootData();
          $scope.data = {step:3,mode:'Add',showblank: true};
          $mdToast.show(
            $mdToast.simple()
              .content('Leave updated')
              .hideDelay(3000)
          );
        },function(error){
          console.log(error);
        });
      },function(error){
        $mdToast.show(
          $mdToast.simple()
            .content('Failed to update leave')
            .hideDelay(3000)
        );
      });
    };

    $scope.addLeave = function() {
      var data = {
        resourceID: $scope.data.resource.id,
        type: $scope.data.leaveType.type,
        half: $scope.data.half,
        start: $scope.data.start.toISOString(),
        end: $scope.data.end.toISOString()
      };
      Leave.createLeave({"data":data},function(success){
        $scope.localData.bootData();
        $scope.data = {step:3,mode:'Add',showblank: true};
        $mdToast.show(
          $mdToast.simple()
            .content('Please select an employee!')
            .hideDelay(3000)
        );
      },function(error){
        console.log(error);
      });
    };
  });
