'use strict';
describe('Controller: resourceCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var resourceCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    resourceCtrl = $controller('resourceCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
