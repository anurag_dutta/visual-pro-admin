'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resetCtrl
 * @description
 * # resetCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('resetCtrl', function ($scope,$state,$stateParams, $mdDialog,User) {
  	$scope.credentials = {};
  	$scope.$on('$stateChangeSuccess',function(){
  		if(!$stateParams.accessToken) {
  			$scope.unauthorized = true;
  		}
  	});

  	$scope.reset = function(creds) {
  		if(!creds.password) {
  			showToast("Please enter your new password");
  		}
  		else if(creds.password!=creds.confirm) {
  			showToast("Passwords don't match");
  		}
  		else {
  			User.changePass({"token":$stateParams.accessToken,"password":creds.password},function(success){
  				if(success.result.message) {
  					showToast(success.result.message);
  				}
  				else {
  					showToast("Password changed successfully. You can now use your new password to login");
  					$scope.showRedirect = true;
  				}
  			},function(error){
  				console.log(error);
  			});
  		}
  	};

  	function showToast(message) {
  	  $scope.alert = $mdDialog.alert({
  	    title: 'Alert',
  	    content: message,
  	    ok: 'Ok'
  	  });
  	  $mdDialog
  	    .show( $scope.alert );
  	}
  });
