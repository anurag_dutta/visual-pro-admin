'use strict';
describe('Controller: resetCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var resetCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    resetCtrl = $controller('resetCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
