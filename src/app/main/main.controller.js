'use strict';

angular.module('ratadmin')
  .controller('MainCtrl', function ($scope,$mdSidenav,User,$state,localUserFactory,localStorageService,$location, $mdDialog) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.credentials = {};
    $scope.doLogin = function(credentials) {
      $scope.loginRunning = true;
      User.login({rememberMe:true},credentials,function(result){
        if(result.$resolved) {
          localUserFactory.setUser(result.user,result.id);
          $scope.loginRunning = false;
          if(localUserFactory.roles.admin) {
            $state.go('bookings');
          }
          else if(localUserFactory.roles.resource) {
            $state.go('resourcedashboard');
          }
          else {
            $state.go('dashboard');
          }
        }
        else {
          $scope.loginRunning = false;
          console.log(err);
        }
      },function(error){
        $scope.loginRunning = false;
        $scope.alert = $mdDialog.alert({
          title: 'Error',
          content: 'Failed to login. Please check your email or password',
          ok: 'Close'
        });
        $mdDialog
          .show( $scope.alert )
          .finally(function() {
            $scope.alert = undefined;
          });
      });
    };
    $scope.doLogout = function() {
      Vsresource.logout(function(result){
        if(result.$resolved) {
          localUserFactory.userID = null;
          $state.go('login');
        }
        else {
          console.log("Logout Failed");
        }
      });
    };
  });
