'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('employeeCtrl', function ($scope,$mdSidenav,localDataService,$mdDialog,localUserFactory,$state,User,SignupRequest) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.data = {step:0,mode:'Add'};
    $scope.localData = localDataService;
    $scope.newEmployee = {};
	$scope.accept = function(request) {
		$scope.newEmployee = {
			email: request.email,
			password: request.password,
			requestID: request.id
		};
		$scope.confirmPassword = request.password;
		$scope.data.step=2;
		$scope.data.requestID = request.id;
	}; 

	$scope.setOrderParam = function(param) {
	  if($scope.orderParam==param) {
	    $scope.orderParam = '-'+param;
	  }
	  else {
	    $scope.orderParam = param;
	  }
	};
	
	$scope.decline = function(request) {
		var confirm = $mdDialog.confirm()
		      .title('Are you sure you want to decline this request?')
		      .content('This is not a reversible process.')
		      .ariaLabel('Decline Request')
		      .ok('Yes')
		      .cancel('Cancel');
		 $mdDialog.show(confirm).then(function(success){
		 	SignupRequest.deleteById({"id":request.id},function(success){
		 		console.log("Request declined");
		 	},function(failure){
		 		$scope.alert = $mdDialog.alert({
		 		  title: 'Failed',
		 		  content: 'Could not decline request',
		 		  ok: 'Ok'
		 		});
		 		$mdDialog
		 		  .show( $scope.alert )
		 		  .finally(function() {
		 		    $scope.alert = undefined;
		 		    $scope.localData.bootData();
		 		});
		 	});
		 });
	};
	$scope.doLogout = function() {
      User.logout(function(result){
        if(result.$resolved) {
         localUserFactory.authentication = null;
         $state.go('login');
        }
        else {
          console.log("Logout Failed");
        }
      });
    };
	$scope.createEmployee = function() {
		if($scope.data.mode=='Edit') {
			var updatedEmployee = _.omit($scope.newEmployee,'id');
			User.updateAll({"where":{id:$scope.newEmployee.id}},updatedEmployee,function(result){
				$scope.alert = $mdDialog.alert({
				  title: 'Success',
				  content: 'Updated user details',
				  ok: 'Ok'
				});
				$mdDialog
				  .show( $scope.alert )
				  .finally(function() {
				    $scope.alert = undefined;
				    $scope.data = {step:0,mode:'Add'};
				    $scope.newEmployee = {};
				    $scope.localData.bootData();
				});
			},function(err){
				console.log(err);
			});
		}
		else {
			var requestID = $scope.newEmployee.requestID;
			$scope.newEmployee = _.omit($scope.newEmployee,'requestID');
			$scope.newEmployee.roles = ["employee"];
			User.create($scope.newEmployee,function(result){
				if(result.$resolved) {
					if(requestID) {
						SignupRequest.deleteById({"id":requestID},function(result){
							console.log(result);
							$scope.data = {step:0,mode:'Add'};
							$scope.localData.bootData();
							// console.log("Created new employee",result.user.firstName);
						});
					}
					else {
						console.log(result);
						$scope.data = {step:0,mode:'Add'};
						$scope.localData.bootData();
					}
					$scope.alert = $mdDialog.alert({
					  title: 'Success',
					  content: 'User added successfully. Login details have been mailed to the provided email address!',
					  ok: 'Close'
					});
					$mdDialog
					  .show( $scope.alert )
					  .finally(function() {
					    $scope.alert = undefined;
					});
					
				}
				else {
					console.error("Could not create user",result);
					$scope.alert = $mdDialog.alert({
					  title: 'Attention',
					  content: 'Could not create user!',
					  ok: 'Close'
					});
					$mdDialog
					  .show( $scope.alert )
					  .finally(function() {
					    $scope.alert = undefined;
					});
				}
			});
		}
		
	};
	
	$scope.delete = function(employee) {
		var confirm = $mdDialog.confirm()
		      .title('Are you sure you want to remove user: '+employee.firstName+' '+employee.lastName+'?')
		      .content('This is not a reversible process.')
		      .ariaLabel('Delete Employee')
		      .ok('Yes')
		      .cancel('Cancel');
		    $mdDialog.show(confirm).then(function() {
		      User.deleteById({"id":employee.id},function(result){
		      	if(result.$resolved) {
		      		var message = "User deleted successfully";
		      	}
		      	else {
		      		var message = "Failed to delete user. Please try again";
		      	}
		      	$scope.localData.bootData();
		      	var alert = $mdDialog.alert({
		      		content: message,
		      		ok: 'Ok'
		      	});
		      	$mdDialog
		      	  .show( alert )
		      	  .finally(function() {
		      	    alert = undefined;
		      	});
		      });
		    }, function() {
		      $scope.alert = 'You decided to keep your debt.';
		    });
	};

	$scope.edit = function(user) {
		$scope.newEmployee = user;
		$scope.data.mode = 'Edit';
		$scope.data.step = 2;
	};
  });
