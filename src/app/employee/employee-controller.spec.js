'use strict';
describe('Controller: employeeCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var employeeCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    employeeCtrl = $controller('employeeCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
