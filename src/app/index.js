'use strict';

angular.module('ratadmin', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial','ngMdIcons','lbServices','ngMessages','LocalStorageModule','ui.calendar','ng.deviceDetector'])
  .config(function ($stateProvider, $urlRouterProvider,$httpProvider, localStorageServiceProvider,$mdThemingProvider, $locationProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('green')
        .accentPalette('orange');
    $stateProvider
      .state('login', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'app/dashboard/dashboard.html',
        controller: 'DashCtrl'
      })
      .state('download', {
        url: '/download',
        templateUrl: 'app/download/download.html',
        controller: 'downloadCtrl'
      })
      .state('resourcedashboard', {
        url: '/resourcedashboard',
        templateUrl: 'app/dashboard/resource-dashboard.html',
        controller: 'DashCtrl'
      })
      .state('resource', {
        url: '/resource',
        templateUrl: 'app/resource/resource.html',
        controller: 'resourceCtrl'
      })
      .state('employee', {
        url: '/employee',
        templateUrl: 'app/employee/employee.html',
        controller: 'employeeCtrl'
      })
      .state('slots', {
        url: '/slots',
        templateUrl: 'app/slots/slots.html',
        controller: 'slotsCtrl'
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/settings/settings.html',
        controller: 'settingsCtrl'
      })
      .state('calendars', {
        url: '/calendars',
        templateUrl: 'app/calendars/calendars.html',
        controller: 'calendarsCtrl',
      })
      .state('reset', {
        url: '/reset?accessToken',
        templateUrl: 'app/reset/reset.html',
        controller: 'resetCtrl'
      })
      .state('bookings', {
        url: '/bookings',
        templateUrl: 'app/bookings/bookings.html',
        controller: 'bookingsCtrl'
      });

    $urlRouterProvider.otherwise('/');
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function($q, $location, LoopBackAuth) {
      return {
        responseError: function(rejection) {
          if (rejection.status == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/login');
          }
          return $q.reject(rejection);
        }
      };
    });
    localStorageServiceProvider
        .setPrefix('ratadmin');
  })
  .filter('timeHumanize',function(){
    return function(input) {
      input = parseInt(input);
      if(input>12) {
        return (input-12)+'pm';
      }
      else {
        return input+'am';
      }
    };
  })
  .filter('arrayToTime',function(){
    return function(input) {
      var hour = input[0];
      var minute = input[1];
      var suffix = 'am';
      if(hour>=12) {
        suffix = 'pm';
        hour = hour-12;
      }
      if(hour<10) {
        hour = '0'+hour;
      }
      if(minute<10) {
        minute = '0'+minute;
      }
      return hour+':'+minute+' '+suffix;
    };
  })
  .filter('onlyPast', function () {
      return function (slots) {
      	var today = moment();
        slots = _.filter(slots,function(slot){
        	// var date = moment(slot[0].date,moment.ISO_8601);
        	// if(date.isBefore(today,'day')) {
        	// 	return true;
        	// }
        	// else {
        	// 	return false;
        	// }
          if(slot[0].status=='open') {
            return false;
          }
          else {
            return true;
          }
        });
        return slots;
      };
  })
  .filter('noPast', function () {
      return function (slots) {
      	var today = moment();
        slots = _.filter(slots,function(slot){
        	var date = moment(slot[0].date,moment.ISO_8601);
        	if(date.isBefore(today,'day')) {
        		return false;
        	}
        	else {
        		return true;
        	}
        });
        return slots;
      };
  })
  .filter('zpad', function() {
    return function(input, n) {
      if(input === undefined)
        input = ""
      if(input.length >= n)
        return input
      var zeros = "0".repeat(n);
      return (zeros + input).slice(-1 * n)
    };
  })
  .filter('todayFirst', function () {
      return function (slots) {
      	var today = moment();
        slots = _.sortBy(slots,function(slot){
        	var date = moment(slot[0].date,moment.ISO_8601);
        	if(date.isSame(today,'day')) {
        		return -999;
        	}
        	else {
        		return (today.diff(date,'days'));
        	}
        });
        return slots;
      };
  })
;
