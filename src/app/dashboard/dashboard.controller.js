'use strict';

angular.module('ratadmin')
  .controller('DashCtrl', function ($scope,$mdSidenav,Booking,$state,localUserFactory,WorkType,$mdDialog,$interval,localDataService,$rootScope,User, $window) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.doLogout = function() {
      User.logout(function(result){
        if(result.$resolved) {
         localUserFactory.authentication = null;
         $state.go('login');
        }
        else {
          console.log("Logout Failed");
        }
      });
    };
    $scope.localData = localDataService;
    $scope.localUser = localUserFactory;

    $scope.data = {step: 2};

    $scope.$on('$stateChangeSuccess',function(event,toState,fromState){
      if(toState.name=='dashboard') {
        $scope.data.step  = 0;
      }
    });

    $scope.onlyFuture = function(item) {
      var today = moment().set({'hour':0,'minute':0,'second':0,'millisecond':0});
      var date = moment(Date.parse(item.start)).set({'hour':0,'minute':0,'second':0,'millisecond':0});
      var diff = today.diff(date,'min',true);
      return diff<=0;
      // var today = moment().startOf('day');
      // var bookDate = moment(item.start,moment.ISO_8601);
      // return (bookDate.isAfter(today, 'day'));
    };
     $scope.onlyPast = function(item) {
      var today = moment().set({'hour':0,'minute':0,'second':0,'millisecond':0});
      var date = moment(Date.parse(item.start)).set({'hour':0,'minute':0,'second':0,'millisecond':0});
      var diff = today.diff(date,'min',true);
      return diff>0;
      // var today = moment();
      // var bookDate = moment(item.start,moment.ISO_8601);
      // return (bookDate.isBefore(today, 'day'));
    };

    function showToast(message) {
      $scope.alert = $mdDialog.alert({
        title: 'Alert',
        content: message,
        ok: 'Ok'
      });
      $mdDialog
        .show( $scope.alert );
    }

    $scope.isToday = function(isoDate) {
      var input = moment(isoDate,moment.ISO_8601);
      return moment().isSame(input,'day');
    };

    $scope.endBooking = function(slot,ev) {
      $mdDialog.show({
        controller: finishBookingCtrl,
        clickOutsideToClose: false,
        templateUrl: 'app/modals/finishBooking.html',
        locals: {
          slot: slot
        }
      })
      .then(function(answer) {
        
      }, function() {
        $scope.alert = 'You cancelled the dialog.';
      });
    };

    $scope.selectSlot = function(index) {
      $scope.selectedSlotIndex = index;
    };

    $scope.modifyBooking = function(slot) {
      $scope.modifying = true;
      $scope.data.date = moment(slot.start, moment.ISO_8601).toDate();
      $scope.data.startTime = moment(slot.start,moment.ISO_8601).toDate();
      $scope.data.endTime = moment(slot.end, moment.ISO_8601).toDate();
      $scope.data.workType = slot.workType;
      $scope.data.bookID = slot.id;
      $scope.data.notes = slot.notes;
      $scope.data.employee = _.find($scope.localData.employees,function(item){
        return item.id == slot.employeeID;
      });
      if($scope.data.employee) {
        $scope.employeeSearch = $scope.data.employee.firstName+' '+$scope.data.employee.lastName;
      }
      else if(slot.employeeID){
        $scope.data.employee = {id:slot.employeeID,firstName:slot.employeeID};
        $scope.employeeSearch = slot.employeeID;
      }
      $scope.data.resource = _.find($scope.localData.resources,function(item){
        return item.id == slot.resourceID;
      });
      if($scope.data.resource) {
        $scope.resourceSearch = $scope.data.resource.firstName+' '+$scope.data.resource.lastName;
      }
       else if(slot.resourceID){
        $scope.data.resource = {id:slot.resourceID,firstName:slot.resourceID};
        $scope.resourceSearch = slot.resourceID;
      }
      $scope.data.casecode = slot.casecode;
      $scope.data.step = 0;
    };

    function finishBookingCtrl($scope,$mdDialog,slot) {
      $scope.data = {};
      $scope.finishItUp = function(ev) {
        ev.preventDefault();
        $mdDialog.hide();
        Booking.updateAll({"where":{"id":slot.id}},{"status":"completed"},function(result){
          if(result.$resolved) {
            $scope.alert = $mdDialog.alert({
              title: 'Success',
              content: 'This booking has ended. Feedback has been requested!',
              ok: 'Ok'
            });
            $mdDialog
              .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
              });
          }
          else {
            $scope.alert = $mdDialog.alert({
              title: 'Attention',
              content: 'Failed to end booking. Please contact support!',
              ok: 'Close'
            });
            $mdDialog
              .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
              });
          }
        })
        // Booking.finishBooking({"slot":slot,"resourceInput":{}},function(result){
        //   if(result.$resolved) {
        //     $scope.alert = $mdDialog.alert({
        //       title: 'Success',
        //       content: 'This booking has ended. Feedback has been requested!',
        //       ok: 'Ok'
        //     });
        //     $mdDialog
        //       .show( $scope.alert )
        //       .finally(function() {
        //         $scope.alert = undefined;
        //       });
        //   }
        //   else {
        //     $scope.alert = $mdDialog.alert({
        //       title: 'Attention',
        //       content: 'Failed to end booking. Please contact support!',
        //       ok: 'Close'
        //     });
        //     $mdDialog
        //       .show( $scope.alert )
        //       .finally(function() {
        //         $scope.alert = undefined;
        //       });
        //   }
        // });
      };
    };

    $scope.cancelBooking = function(item) {
      var confirm = $mdDialog.confirm()
            .parent(angular.element(document.body))
            .title('Are you sure you want to cancel this appointment?')
            .ariaLabel('Cancel appointment')
            .ok('Yes')
            .cancel('No');
          $mdDialog.show(confirm).then(function() {
            Booking.deleteById({"id":item.id},function(success){
                showToast("Booking canceled succesfully");
                $scope.localData.bootData();
              });
            }, function() {
              showToast("Failed to  cancel booking");
            });
          //   Booking.updateAll({"where":{"id":item.id}},{"status":"cancelled"},function(success){
          //     showToast("Booking canceled succesfully");
          //     $scope.localData.bootData();
          //   });
          // }, function() {
          //   showToast("Failed to  cancel booking");
          // });
    };

    $scope.employeesearch = function(query) {
      query = angular.lowercase(query);
      var result = _.filter($scope.localData.employees,function(employee){
        var test = angular.lowercase(employee.firstName)+angular.lowercase(employee.lastName);
        return (test.indexOf(query)===0);
      });
      $scope.employeeSearch = query;
      return result;
    };
     $scope.resourcesearch = function(query) {
      query = angular.lowercase(query);
      var result = _.filter($scope.localData.resources,function(resource){
        var test = angular.lowercase(resource.firstName)+angular.lowercase(resource.lastName);
        return (test.indexOf(query)===0);
      });
      return result;
    };

    $scope.findEmployee = function(id) {
      if(id=="" || !id) {
        return "Self Booking";
      } 
      var employee = _.find($scope.localData.employees,function(item){
        return item.id==id;
      });
      if(employee) {
        return employee.firstName+' '+employee.lastName;
      }
      else {
        return id;
      }
    };

    $scope.findSlots = function(data) {
      if(!data.date || !data.workType || !data.hours || !data.casecode) {
        $scope.alert = $mdDialog.alert({
          title: 'Attention',
          content: 'All fields are required!',
          ok: 'Close'
        });
        $mdDialog
          .show( $scope.alert )
          .finally(function() {
            $scope.alert = undefined;
          });
      }
      else if(moment(data.date).day()==0 || moment(data.date).day()==6) {
        var confirm = $mdDialog.confirm()
          .parent(angular.element(document.body))
          .title('You have selected a weekend.')
          .content('To book on a weekend, please send us an email.')
          .ariaLabel('Weekend')
          .ok('Email us')
          .cancel('Choose another day')
        $mdDialog.show(confirm).then(function() {
          $window.location="mailto:indiaproduction@bcg.com";
        }, function() {
          
        });
      }
      else {
        Booking.getAvailableSlots({"type":data.workType,"casecode":data.casecode,"hours":data.hours,"date":moment(data.date).toISOString()},function(result){
          if(result.$resolved) {
            if(result.slots.message) {
              $scope.alert = $mdDialog.alert({
                title: 'Attention',
                content: result.slots.message,
                ok: 'Close'
              });
              $mdDialog
                .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
              });
            }
            else {
              $scope.slots = result.slots;
              $scope.foundSlots = true;
              $scope.data.step =1;
            }
          }
        },function(error){
          if(!error.found) {
            $scope.alert = $mdDialog.alert({
              title: 'Attention',
              content: 'No slots available on this date. Please choose another date!',
              ok: 'Close'
            });
            $mdDialog
              .show( $scope.alert )
            .finally(function() {
              $scope.alert = undefined;
            });
          }
        });
      }
    };

    $scope.setOrderParam = function(param) {
      if($scope.orderParam==param) {
        $scope.orderParam = '-'+param;
      }
      else {
        $scope.orderParam = param;
      }
    };

    $scope.bookSlots = function() {
      var slot = {};
      slot.employeeID = $scope.localUser.userData.id;
      slot.workType = $scope.data.workType;
      slot.bookedOn = moment().toISOString();
      slot.casecode = $scope.data.casecode;
      slot.hours = $scope.data.hours;
      slot.resourceID = $scope.slots[$scope.selectedSlotIndex].resources[0].resource;
      slot.shiftID = $scope.slots[$scope.selectedSlotIndex].resources[0].shift;
      slot.status = 'upcoming';
      slot.start = $scope.slots[$scope.selectedSlotIndex].start;
      slot.end = $scope.slots[$scope.selectedSlotIndex].end;
      slot.owner = {"type":"employee","userID":$scope.localUser.userData.id};
      Booking.create(slot,function(result){
        if(result.$resolved) {
          $scope.alert = $mdDialog.alert({
            title: 'Success',
            content: 'The booking has been confirmed!',
            ok: 'Close'
          });
          $mdDialog
            .show( $scope.alert )
            .finally(function() {
              $scope.alert = undefined;
              $scope.data = {step:0};
              bookingInit();
            });
        }
      });
    };

    $scope.confirmBooking = function(data) {
      if(!data.date || !data.workType || !data.startTime || !data.endTime || !data.casecode) {
        $scope.alert = $mdDialog.alert({
          title: 'Attention',
          content: 'All fields are required!',
          ok: 'Close'
        });
        $mdDialog
          .show( $scope.alert )
          .finally(function() {
            $scope.alert = undefined;
          });
        return;
      }
      else if(!data.resource) {
        showToast("You must select a resource. Use the auto-comeplete to select one.");
      }
      var start = moment(data.date).set({
        "hour":moment(data.startTime).get('hour'),
        "minute":moment(data.startTime).get('minute'),
        "second":moment(data.startTime).get('second'),
        "millisecond":moment(data.startTime).get('millisecond')
      });
      var end = moment(data.date).set({
        "hour":moment(data.endTime).get('hour'),
        "minute":moment(data.endTime).get('minute'),
        "second":moment(data.endTime).get('second'),
        "millisecond":moment(data.endTime).get('millisecond')
      });
      var booking = {
        "start": start.toISOString(),
        "end": end.toISOString(),
        "resourceID":data.resource.id,
        "workType": data.workType,
        "casecode": data.casecode,
        "notes": data.notes,
        "owner": {
          "type":"resource",
          "userID":$scope.localUser.userData.id
        }
      };
      if(data.employee) {
        booking.employeeID = data.employee.id;
      }
      else {
        booking.employeeID = $scope.employeeSearch;
      }
      if($scope.modifying) {
        Booking.updateAll({"where":{"id":data.bookID}},booking,function(success){
          showToast("Booking updated successfully");
          // $scope.$destroy();
          $scope.localData.bootData();
          $scope.data= {step:0};
          $scope.modifying =false;
        }, function(error){
          showToast("Could not complete booking. Are you connected to the internet?");
        });
      }
      else {
        Booking.adminBook({"booking":booking},function(success){
          if(success.status.message) {
            showToast(success.status.message);
            $scope.data= {step:0};
          }
          else {
            showToast("Booking created successfully");
            // $scope.$destroy();
            $scope.localData.bootData();
            $scope.data= {step:0};
          }
        }, function(error){
          showToast("Could not complete booking. Are you connected to the internet?");
        });
      }
    };

    function bookingInit(){
       
        // Booking.getAll(function(result){
        //   $scope.allBookings = result.bookings;
        // });
    }
    
  });
