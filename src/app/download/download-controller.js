'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:downloadCtrl
 * @description
 * # downloadCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('downloadCtrl', function ($scope, deviceDetector, $window) {
  	$scope.urls = {
  		'android':'http://anurag.ekcutting.in/IndiaVS.apk',
  		'ios': 'https://ota.io/cppyvf'
  	};
  	$scope.$on('$stateChangeSuccess',function(){
  		$scope.data = {os:deviceDetector.os};
  	});
    $scope.download = function(){
    	$window.location.href = $scope.urls[$scope.data.os];
    };
  });
