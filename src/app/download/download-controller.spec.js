'use strict';
describe('Controller: downloadCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var downloadCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    downloadCtrl = $controller('downloadCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
