'use strict';
describe('Controller: slotsCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var slotsCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    slotsCtrl = $controller('slotsCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
