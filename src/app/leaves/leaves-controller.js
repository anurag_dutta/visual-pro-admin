'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('leavesCtrl', function ($scope,$mdSidenav,localDataService) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.localData = localDataService;
  });
