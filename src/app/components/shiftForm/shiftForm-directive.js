'use strict';
/**
 * @ngdoc directive
 * @name ratadmin.directive:shiftForm
 * @description
 * # shiftForm
 */
angular.module('ratadmin')
  .directive('shiftForm', function () {
    return {
      templateUrl: 'app/components/shiftForm/shiftForm.html',
      restrict: 'E',
      scope: {
      	'resource':'=',
      	'edits': '='
      },
      link: function(scope, element, attrs,$timeout) {
      	if(scope.edits) {
      		scope.data = {step:2};
      	}
      	else {
      		scope.data={step:0};
	      	scope.newShift = {resourceID:scope.resource};
      	}
      	scope.element = element;
      },
      controller: function($scope,localDataService, $mdToast, Shift) {
      	$scope.localData = localDataService;
      	$scope.choosePeriod = function(period) {
      		var today = moment().startOf(period).add(1,period);
      		var from = today.toDate();
      		$scope.data.start = from;
      		console.log(today.toDate());
      		var to = moment().startOf(period).add(2,period);
      		$scope.data.end = to.toDate();
      	};
      	$scope.$on('edit-mode',function(){
      		$scope.data.step = 2;
      	});

      	function parseTime(input) {
      	  var result = input.split(':');
      	  result[0] = parseInt(result[0]);
      	  result[1] = result[1].split(' ');
      	  if(result[1][1]=='pm') {
      	    result[0] = result[0]+12;
      	  }
      	  result[1] = parseInt(result[1][0]);
      	  return result;
      	}

      	$scope.goToDates = function() {
      		if(!$scope.newShift.resourceID) {
      			$mdToast.show(
  			      $mdToast.simple()
  			        .content('Please select an employee!')
  			        .hideDelay(3000)
  			    );
      		}
      		else if(!$scope.data.period) {
      			$mdToast.show(
  			      $mdToast.simple()
  			        .content('Please select a time period!')
  			        .hideDelay(3000)
  			    );
      		}
      		else {
      			$scope.data.step = 1;
      		}
      	};

      	$scope.selectDates = function() {
      		if(!$scope.data.start) {
      			$mdToast.show(
  			      $mdToast.simple()
  			        .content('Please select a start date!')
  			        .hideDelay(3000)
  			    );
      		}
      		else if(!$scope.data.end) {
      			$mdToast.show(
  			      $mdToast.simple()
  			        .content('Please select an end date!')
  			        .hideDelay(3000)
  			    );
      		}
      		else {
      			$scope.newShift.start = moment($scope.data.start).toISOString();
      			$scope.newShift.end = moment($scope.data.end).toISOString();
      			$scope.data.step = 2;
      		}
      	};

      	$scope.createShift = function() {
      		if(!$scope.data.startTime || !$scope.data.endTime || !$scope.data.break1 ||!$scope.data.break2) {
      			$mdToast.show(
  			      $mdToast.simple()
  			        .content('All fields are compulsory!')
  			        .hideDelay(3000)
  			    );
      		}
      		else {
      			$scope.newShift.startTime = parseTime($scope.data.startTime);
      			$scope.newShift.endTime = parseTime($scope.data.endTime);
      			$scope.newShift.breaks = [parseTime($scope.data.break1),parseTime($scope.data.break2)];
      			// $scope.newShift = _.omit($scope.newShift,['resourceID']);
      			if($scope.edits) {
      				Shift.updateAll({"where":{id:{inq:$scope.edits.changingShifts}}},{startTime:$scope.newShift.startTime,endTime:$scope.newShift.endTime,breaks:$scope.newShift.breaks},function(success){
		    			$mdToast.show(
					      $mdToast.simple()
					        .content('Shift created successfully!')
					        .hideDelay(3000)
					    );
					    $scope.$emit('close-form');
      				},function(error){
		    			$mdToast.show(
					      $mdToast.simple()
					        .content('Failed to create shift. Please try again!')
					        .hideDelay(3000)
					    );
      				});
      			}
      			else {
	      			$scope.newShift.period = $scope.data.period;
	      			Shift.createShift({"shift":$scope.newShift},function(success){
  		    			$mdToast.show(
  					      $mdToast.simple()
  					        .content('Shift created successfully!')
  					        .hideDelay(3000)
  					    );
                $scope.data.step = 0;
                $scope.newShift = {};
  					    $scope.$emit('close-form');
	      			},function(error){
  		    			$mdToast.show(
  					      $mdToast.simple()
  					        .content('Failed to create shift. Please try again!')
  					        .hideDelay(3000)
  					    );
  					    $scope.data.step = 0;
	      			});
      			}
      		}
      	};
      }
    };
  });
