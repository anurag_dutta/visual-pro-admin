'use strict';
/**
 * @ngdoc directive
 * @name ratadmin.directive:sideNav
 * @description
 * # sideNav
 */
angular.module('ratadmin')
  .directive('sidenav', function () {
    return {
      templateUrl: 'app/components/sideNav/sideNav.html',
      restrict: 'E',
      link: function(scope, element, attrs) {
      },
      controller: function($scope,$mdSidenav,$rootScope, localUserFactory) {
        $scope.localUser = localUserFactory;
      }
    };
  });
