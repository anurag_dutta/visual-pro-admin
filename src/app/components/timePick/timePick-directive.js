'use strict';
/**
 * @ngdoc directive
 * @name ratadmin.directive:timePick
 * @description
 * # timePick
 */
angular.module('ratadmin')
  .directive('timePick', function () {
    return {
      templateUrl: 'app/components/timePick/timePick.html',
      restrict: 'E',
      scope: {
      	'label':'@',
      	'selected': '='
      },
      link: function(scope, element, attrs) {
       	$(element).find('input').pickatime({
       		onSet: function(data) {
       			var temp = this.get();
       			if(temp)
	       			scope.selected = temp;
       			scope.$apply();
       		}
       	});
      }
    };
  });
