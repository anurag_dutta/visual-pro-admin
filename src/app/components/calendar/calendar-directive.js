'use strict';
/**
 * @ngdoc directive
 * @name ratadmin.directive:calendar
 * @description
 * # calendar
 */
angular.module('ratadmin')
  .directive('calendar', function () {
    return {
      templateUrl: 'app/components/calendar/calendar.html',
      restrict: 'E',
      scope: {
      	selecteddate: '=',
      	dateChanged: '&datechanged',
            disabledayselect: '@',
            monthchanged: '&monthchanged'
      },
      link: function(scope, element, attrs) {
      	generateCalendar(moment().get('month'));
      	function generateCalendar(month) {
      		scope.currentMonth = moment().set({'month':month});
      		scope.month = scope.currentMonth.format('MMM');
      		var counter = moment(scope.currentMonth.toDate()).startOf('month').startOf('week');
      		var monthEnd = moment(scope.currentMonth.toDate()).endOf('month').endOf('week');
      		var days = [];
      		while(counter.isBefore(monthEnd)) {
      			days.push({
      				date: counter.toISOString(),
      				day: counter.day(),
      				month: counter.get('month'),
      				week: counter.week(),
      				isBefore: counter.isBefore(moment(),'day')
      			});
      			counter.add(1,'days');
      		}
      		scope.days = _.groupBy(days,function(day){return day.week;});
      		console.log(scope.days);
      	}

      	scope.nextMonth = function() {
      		scope.currentMonth.add(1,'months');
      		generateCalendar(scope.currentMonth.get('month'));
                  scope.monthchanged({month:scope.currentMonth.get('month')+1});
      	};
      	scope.prevMonth = function() {
      		scope.currentMonth.subtract(1,'months');
      		generateCalendar(scope.currentMonth.get('month'));
                  scope.monthchanged({month:scope.currentMonth.get('month')+1});
      	};
      },
      controller: function($scope) {
      	$scope.selectDate = function(day) {
                  if(!$scope.disabledayselect) {
                        $scope.selecteddate = day.date;
                        $scope.dateChanged({date:day.date});
                  }
      	}
      }
    };
  });
