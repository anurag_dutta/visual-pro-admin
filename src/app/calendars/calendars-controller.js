'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('calendarsCtrl', function ($scope,$mdSidenav,localDataService, localUserFactory, User, $state, $timeout) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.localData = localDataService;
    $scope.localUser = localUserFactory;

    $scope.findEmployee = function(id,slot) {
      if(!id)
        return "Self Booking";
      var result = _.find($scope.localData.employees,function(item){
        return item.id==id;
      });
      if(result) {
        return result.firstName+' '+result.lastName;
      }
      else  {
        var result = _.find($scope.localData.resources,function(item){
          return item.id==id;
        });
        if(result) {
          return result.firstName+' '+result.lastName;
        }
        else
          return id;
      }
    };

    $scope.doLogout = function() {
      User.logout(function(result){
        if(result.$resolved) {
         
        }
        else {
          console.log("Logout Failed");
        }
      });
      localUserFactory.authentication = null;
      $state.go('login');
    };

    $scope.data = {events:[],currentUserList:[],employees:[],resources:[],date:moment().startOf('day').toISOString(),selectedLocation:''};
    if($scope.localData.events) {
      $scope.data.events.push($scope.localData.events);
    }
    $scope.$on('$stateChangeSuccess',function(){
      $scope.searching = true;
      $scope.localData.getCalendar().then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    });

    $scope.selectLocation = function(location) {
      $scope.data.selectedLocation=location;
    };

    $scope.$on('$stateChangeStart',function(){
        $timeout(function(){
          $scope.$destroy();
        });
    });

    $scope.nextDay = function() {
      var day = moment($scope.data.currentDay).add(1,'days');
      $scope.data.currentDay = day.toDate();
      $scope.searching = true;
      $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    };
    $scope.changeDate = function(date) {
      $scope.data.currentDay = date;
      $scope.searching = true;
      $scope.localData.selectedDate = $scope.data.currentDay;
      $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    };
    $scope.prevDay = function() {
      var day = moment($scope.data.currentDay).subtract(1,'days');
      $scope.data.currentDay = day.toDate();
      $scope.searching = true;
            $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
              $scope.searching = false;
            },function(error){
              $scope.searching = false;
            });
    };

    $scope.userChanged = function(user,status) {
      _.each($scope.localData.resources,function(resource){
          if(resource.id==user.id) {
            console.log("Found user");
            resource.selected = status;
          }
      });
    };

    $scope.moveResourceDown = function(resource) {
        if(resource.seq==$scope.localData.resources.length) {
            return;
        }
        var nextIndex,currentIndex;
        for(var i=0;i<$scope.localData.resources.length;i++) {
            var item = $scope.localData.resources[i];
            if(item.seq==resource.seq) {
                currentIndex = i;
            }
            if(item.seq==(resource.seq+1)) {
                nextIndex = i;
            }
        }
        

        $scope.localData.resources[nextIndex].seq = resource.seq;
        $scope.localData.resources[currentIndex].seq = resource.seq+1;
    };

    $scope.moveResourceUp = function(resource) {
        if(resource.seq==1) {
            return;
        }
        var previousIndex,currentIndex;
        for(var i=0;i<$scope.localData.resources.length;i++) {
            var item = $scope.localData.resources[i];
            if(item.seq==resource.seq) {
                currentIndex = i;
            }
            if(item.seq==(resource.seq-1)) {
                previousIndex = i;
            }
        }
        
        $scope.localData.resources[previousIndex].seq = resource.seq;
        $scope.localData.resources[currentIndex].seq = resource.seq-1;
        
    };
  });
