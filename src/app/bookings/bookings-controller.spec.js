'use strict';
describe('Controller: bookingsCtrl', function () {
// load the controller's module
  beforeEach(module('ratadmin'));
  var bookingsCtrl,
      scope;
// Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    bookingsCtrl = $controller('bookingsCtrl', {
      $scope: scope
    });
  }));
  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
