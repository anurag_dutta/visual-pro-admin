'use strict';
/**
 * @ngdoc function
 * @name ratadmin.controller:resourceCtrl
 * @description
 * # resourceCtrl
 * Controller of the ratadmin
 */
angular.module('ratadmin')
  .controller('bookingsCtrl', function ($scope,$mdSidenav,localDataService,$mdDialog,$rootScope, uiCalendarConfig, localUserFactory,$timeout , User, $state, $mdToast, Shift) {
    $scope.openLeftMenu = function() {
      $mdSidenav('left').toggle();
    };
    $scope.localData = localDataService;

    $(function () {
        $('.antiscroll-wrap').antiscroll();
      });
    $scope.data = {events:[],currentUserList:[],employees:[],resources:[],date:moment().startOf('day').toISOString(),selectedLocation:''};
    if($scope.localData.events) {
      $scope.data.events.push($scope.localData.events);
    }
    $scope.$on('$stateChangeSuccess',function(){
      $scope.searching = true;
      $scope.localData.getCalendar().then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    });

    $scope.selectLocation = function(location) {
      $scope.data.selectedLocation=location;
    };

    $scope.$on('$stateChangeStart',function(){
        $timeout(function(){
          $scope.$destroy();
        });
    });
    $rootScope.$on('booking-modified',function(){
      $scope.searching = true;
      $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    });
    $scope.nextDay = function() {
      var day = moment($scope.data.currentDay).add(1,'days');
      $scope.data.currentDay = day.toDate();
      $scope.searching = true;
      $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    };
    $scope.changeDate = function(date) {
      $scope.data.currentDay = date;
      $scope.searching = true;
      $scope.localData.selectedDate = $scope.data.currentDay;
      $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
        $scope.searching = false;
      },function(error){
        $scope.searching = false;
      });
    };
    $scope.prevDay = function() {
      var day = moment($scope.data.currentDay).subtract(1,'days');
      $scope.data.currentDay = day.toDate();
      $scope.searching = true;
            $scope.localData.getCalendar($scope.data.currentDay).then(function(success){
              $scope.searching = false;
            },function(error){
              $scope.searching = false;
            });
    };

    $scope.userChanged = function(user,status) {
      _.each($scope.localData.resources,function(resource){
          if(resource.id==user.id) {
            console.log("Found user");
            resource.selected = status;
          }
      });
    };

    $scope.doLogout = function() {
      User.logout(function(result){
        if(result.$resolved) {
         
        }
        else {
          console.log("Logout Failed");
        }
      });
      localUserFactory.authentication = null;
      $state.go('login');
    };

    $scope.findEmployee = function(id,slot) {
      if(!id)
        return "Self Booking";
      var result = _.find($scope.localData.employees,function(item){
        return item.id==id;
      });
      if(result) {
        return result.firstName+' '+result.lastName;
      }
      else  {
        var result = _.find($scope.localData.resources,function(item){
          return item.id==id;
        });
        if(result) {
          return result.firstName+' '+result.lastName;
        }
        else
          return id;
      }
    };

    $scope.changeUserList = function() {
      switch($scope.selectedUserType) {
        case 'employees':
          $scope.data.currentUserList = $scope.localData.employees;
          break;
        case 'resources':
          $scope.data.currentUserList = $scope.localData.resources;
          break;
      }
      $scope.selectAll($scope.data.currentUserList);
    };

    $scope.selectAll = function(list) {
      _.each(list,function(item){
        item.selected = true;
      });
    };

    $scope.viewDetails = function(slot) {
      $mdDialog.show({
        controller: bookingDetailCtrl,
        templateUrl: 'app/modals/bookingDetail.html',
        locals: {
          'event':slot,
          'localData':$scope.localData,
          'createCtrl':$scope.createBookingCtrl
        }
      })
      .then(function(answer) {
        
      }, function() {
        
      });
    };

    $scope.eventClick = function(event,ev,view) {
      console.log(event);
      $mdDialog.show({
        controller: bookingDetailCtrl,
        templateUrl: 'app/modals/bookingDetail.html',
        locals: {
          'event':event,
          'localData':$scope.localData,
          'createCtrl':$scope.createBookingCtrl
        }
      })
      .then(function(answer) {
        
      }, function() {
        
      });
    };
    $scope.dayClick = function(day,ev,view) {
      console.log(day);
    };

    $scope.uiConfig = {
      calendar:{
        editable: true,
        header:{
          left: 'month basicDay',
          center: 'title',
          right: 'today prev,next'
        },
        views: {
          basicDay : {
            slotDuration: '01:00:00',
            minTime: '09:00:00',
            maxTime: '18:00:00',
            slotEventOverlap: false
          }
        },
        eventClick: $scope.eventClick,
        dayClick: $scope.dayClick
      }
    };
    $scope.startNewBooking = function() {
      $mdDialog.show({
        controller: $scope.createBookingCtrl,
        templateUrl: 'app/modals/adminBook.html',
        locals: {
          'event': null,
          'localData': $scope.localData,
          'mode': 'Create'
        }
      })
      .then(function(answer) {
          
      }, function() {
        
      });
    };

    function bookingDetailCtrl($scope,$mdDialog,event,localData,createCtrl, Booking,$rootScope) {
      $scope.event = event;
      $scope.event.start = moment($scope.event.start).toDate();
      $scope.event.end = moment($scope.event.end).toDate();
      $scope.event.employeeData = _.find(localData.employees,function(employee){
        return employee.id==$scope.event.details.employeeID;
      });
      $scope.event.resourceData = _.find(localData.resources,function(resource){
        return resource.id==$scope.event.details.resourceID;
      });
      $scope.closeDetails = function(){
        $mdDialog.hide();
      };

      $scope.cancel = function(item) {
        var confirm = $mdDialog.confirm()
              .parent(angular.element(document.body))
              .title('Are you sure you want to cancel this appointment?')
              .ariaLabel('Cancel appointment')
              .ok('Yes')
              .cancel('No');
            $mdDialog.show(confirm).then(function() {
              Booking.deleteBook({"id":item.details.bookid},function(success){
                  showToast("Booking canceled succesfully");
                  $rootScope.$broadcast('booking-modified');
                });
              }, function() {
                showToast("Failed to  cancel booking");
              });
      };

      $scope.findEmployee = function(id) {
        if(!id)
          return "Self Booking";
        var result = _.find(localData.employees,function(item){
          return item.id==id;
        });
        if(result) {
          return result.firstName+' '+result.lastName;
        }
        else  {
          var result = _.find(localData.resources,function(item){
            return item.id==id;
          });
          if(result) {
            return result.firstName+' '+result.lastName;
          }
          else
            return id;
        }
      };

      $scope.modify = function(event) {
        $mdDialog.hide().then(function(){
          $mdDialog.show({
            controller: createCtrl,
            templateUrl: 'app/modals/adminBook.html',
            locals: {
              'event': event,
              'localData': localData,
              'mode': 'Modify'
            }
          })
          .then(function(answer) {
              
          }, function() {
            
          });
        });
      };
    }
    $scope.createBookingCtrl = function($scope,$mdDialog,event,localData,mode,Booking,$rootScope,localUserFactory) {
      $scope.localData = localData;
      $scope.localUser = localUserFactory;
      $scope.mode=mode;
     $scope.event = event;
     if(event){
      $scope.data = {
        date: event.start,
        employee: event.employeeData,
        resource: event.resourceData,
        casecode: event.details.casecode,
        hours: event.duration/2,
        workType: event.details.type,
        step: 0,
        mode: mode,
        startTime: event.start,
        endTime: event.end,
        bookID: event.details.bookid
      };
      if($scope.data.employee) {
        $scope.employeeSearchText = $scope.data.employee.firstName+' '+$scope.data.employee.lastName;
      }
      else {
        $scope.employeeSearchText = event.details.employeeID;
      }
      $scope.resourceSearchText = $scope.data.resource.firstName+' '+$scope.data.resource.lastName;
     }
     else {
      $scope.data = {
        step:0,
        mode: mode
      }
     }
     // $scope.data.workType = _.find(localData.workTypes,function(type){
     //   return type.type == event.type;
     // });
     console.log($scope.employeeSearchText);
      $scope.resourcesearch = function(query) {
        query = angular.lowercase(query);
        var result = _.filter(localData.resources,function(resource){
          if($scope.data.workType) {
            if(!_.find(resource.types,function(type){if(type){return type.type==$scope.data.workType} return false;})) {
              return false;
            }
          }
          var test = angular.lowercase(resource.firstName)+angular.lowercase(resource.lastName);
          return (test.indexOf(query)===0);
        });
        return result;
      };
      $scope.employeesearch = function(query) {
        query = angular.lowercase(query);
        var result = _.filter(localData.employees,function(employee){
          var test = angular.lowercase(employee.firstName)+angular.lowercase(employee.lastName);
          return (test.indexOf(query)===0);
        });
        return result;
      };
      $scope.selectSlot = function(index) {
        $scope.selectedSlotIndex = index;
      };
      // $scope.bookSlots = function() {
      //   if($scope.data.mode == "Modify") {
      //     var slot = {};
      //     if($scope.data.employee) {
      //       slot.employeeID = $scope.data.employee.id;
      //     }
      //     slot.resourceID = $scope.slots[$scope.selectedSlotIndex].resources[0].resource;
      //     slot.casecode = event.details.casecode;
      //     slot.workType = event.details.type;
      //     slot.shiftID = $scope.slots[$scope.selectedSlotIndex].resources[0].shift;
      //     slot.start = $scope.slots[$scope.selectedSlotIndex].start;
      //     slot.end = $scope.slots[$scope.selectedSlotIndex].end;
      //     Booking.updateAll({where:{id:event.details.bookid}},slot,function(result){
      //         if(result.$resolved) {
      //           $mdDialog.hide().then(function(){
      //             $scope.alert = $mdDialog.alert({
      //               title: 'Success',
      //               content: 'Booking successfully modified!',
      //               ok: 'Close'
      //             });
      //             $mdDialog
      //               .show( $scope.alert )
      //               .finally(function() {
      //                 $scope.alert = undefined;
      //                 $rootScope.$broadcast('booking-modified');
      //               });
      //           });
      //         }
      //     });
      //   }
      //   else {
      //     var slot = {};
      //     if($scope.data.employee) {
      //       slot.employeeID = $scope.data.employee.id;
      //     }
      //     slot.workType = $scope.data.workType;
      //     slot.bookedOn = moment().toISOString();
      //     slot.casecode = $scope.data.casecode;
      //     slot.resourceID = $scope.slots[$scope.selectedSlotIndex].resources[0].resource;
      //     slot.shiftID = $scope.slots[$scope.selectedSlotIndex].resources[0].shift;
      //     slot.status = 'upcoming';
      //     slot.start = $scope.slots[$scope.selectedSlotIndex].start;
      //     slot.end = $scope.slots[$scope.selectedSlotIndex].end;
      //     Booking.create(slot,function(result){
      //       if(result.$resolved) {
      //         $scope.alert = $mdDialog.alert({
      //           title: 'Success',
      //           content: 'The booking has been confirmed!',
      //           ok: 'Close'
      //         });
      //         $mdDialog
      //           .show( $scope.alert )
      //           .finally(function() {
      //             $scope.alert = undefined;
      //             $scope.data = {step:0};
      //             // bookingInit();
      //             $rootScope.$broadcast('booking-modified');
      //           });
      //       }
      //     });
      //   }
      // };

      $scope.confirmBooking = function(data,employee) {
        var start = moment(data.date).set({
          "hour":moment(data.startTime).get('hour'),
          "minute":moment(data.startTime).get('minute'),
          "second":moment(data.startTime).get('second'),
          "millisecond":moment(data.startTime).get('millisecond')
        });
        var end = moment(data.date).set({
          "hour":moment(data.endTime).get('hour'),
          "minute":moment(data.endTime).get('minute'),
          "second":moment(data.endTime).get('second'),
          "millisecond":moment(data.endTime).get('millisecond')
        });
        var booking = {
          "start": start.toISOString(),
          "end": end.toISOString(),
          "resourceID":data.resource.id,
          "workType": data.workType,
          "casecode": data.casecode,
          "notes": data.notes,
          "owner": {
            "type":"resource",
            "userID": $scope.localUser.userData.id
          }
        };
        if(data.employee) {
          booking.employeeID = data.employee.id;
        }
        else {
          booking.employeeID =employee;
        }
        if($scope.mode=='Modify') {
          Booking.updateAll({"where":{"id":data.bookID}},booking,function(success){
            showToast("Booking updated successfully");
            // $scope.$destroy();
            $rootScope.$broadcast('booking-modified');
            $scope.data= {step:0};
            $scope.modifying =false;
          }, function(error){
            showToast("Could not complete booking. Are you connected to the internet?");
          });
        }
        else {
          Booking.adminBook({"booking":booking},function(success){
            if(success.status.message) {
              showToast(success.status.message);
              $mdDialog.hide();
            }
            else {
              showToast("Booking created successfully");
              $rootScope.$broadcast('booking-modified');
            }
          }, function(error){
            showToast("Could not complete booking. Are you connected to the internet?");
          });
        }
      };

      $scope.findSlots = function(data) {
        if(!data.date || !data.workType || !data.hours || !data.casecode) {
          showToast("All fields are required");
        }
        else {
          var temp = data.date;
          temp.setHours(0);
          temp.setMinutes(0);
          temp.setSeconds(0);
          temp.setMilliseconds(0);
          temp = moment(temp);
          if(data.resource) {
            var resource = data.resource.id;
          }
          Booking.getAvailableSlots({"type":data.workType,"hours":data.hours,"date":temp.toISOString(),"resourceID":resource},function(result){
            if(result.$resolved) {
              $scope.slots = result.slots;
              console.log($scope.slots);
              $scope.foundSlots = true;
              $scope.data.step =1;
            }
          },function(error){
            if(error.data.error.holiday) {
              $scope.alert = $mdDialog.alert({
                title: 'Attention',
                content: 'Selected day is a holiday due to '+error.data.error.holiday.description+'. Please select another day or write us an email.',
                ok: 'Close'
              });
              $mdDialog
                .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
              });
            }
            if(!error.found) {
              $scope.alert = $mdDialog.alert({
                title: 'Attention',
                content: 'No slots available on this date. Please choose another date!',
                ok: 'Close'
              });
              $mdDialog
                .show( $scope.alert )
              .finally(function() {
                $scope.alert = undefined;
              });
            }
          });
        }
      };
    };

    $rootScope.$on('events-updated',function(){
      $scope.data.events[1] = $scope.localData.events;
    });

    $scope.bookingFilter = function() {
      var localCopy = $scope.data.events[1];
      if(localCopy.length==0) {
        localCopy = $scope.localData.events;
      }
      var newEvents = [];
      _.each(localCopy,function(event,index){
        var flag = true;
        if($scope.selectedEmployee && $scope.selectedEmployee!="") {
          if(event.employee!=$scope.selectedEmployee.id) {
            flag = false;
          }
        }
        if($scope.selectedResource && $scope.selectedResource!="") {
          if(event.resource!=$scope.selectedResource.id) {
            flag = false;
          }
        }
        if($scope.selectedWorkType && $scope.selectedWorkType!="") {
          if(event.type!=$scope.selectedWorkType) {
            flag = false;
          }
        }
        if(flag) {
          newEvents.push(event);
        }
      });

      $scope.data.events[1] = newEvents;

    };

    $scope.moveResourceDown = function(resource) {
        if(resource.seq==$scope.localData.resources.length) {
            return;
        }
        var nextIndex,currentIndex;
        for(var i=0;i<$scope.localData.resources.length;i++) {
            var item = $scope.localData.resources[i];
            if(item.seq==resource.seq) {
                currentIndex = i;
            }
            if(item.seq==(resource.seq+1)) {
                nextIndex = i;
            }
        }
        
        User.updateAll({"where":{"id":$scope.localData.resources[nextIndex].id}},{"seq":resource.seq},function(success){
            User.updateAll({"where":{"id":$scope.localData.resources[currentIndex].id}},{"seq":(resource.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });

        $scope.localData.resources[nextIndex].seq = resource.seq;
        $scope.localData.resources[currentIndex].seq = resource.seq+1;
    };

    function showToast(message) {
      $scope.alert = $mdDialog.alert({
        title: 'Alert',
        content: message,
        ok: 'Ok'
      });
      $mdDialog
        .show( $scope.alert );
    }

    $scope.moveResourceUp = function(resource) {
        if(resource.seq==1) {
            return;
        }
        var previousIndex,currentIndex;
        for(var i=0;i<$scope.localData.resources.length;i++) {
            var item = $scope.localData.resources[i];
            if(item.seq==resource.seq) {
                currentIndex = i;
            }
            if(item.seq==(resource.seq-1)) {
                previousIndex = i;
            }
        }
        
        User.updateAll({"where":{"id":$scope.localData.resources[previousIndex].id}},{"seq":resource.seq},function(success){
            User.updateAll({"where":{"id":$scope.localData.resources[currentIndex].id}},{"seq":(resource.seq)},function(success){
                showToast("Sequence changed successfully");
            }, function(error){
                console.error("Failed to change second sequence",error);
            });
        }, function(error){
            console.error("Failed to change first sequence",error);
        });
        $scope.localData.resources[previousIndex].seq = resource.seq;
        $scope.localData.resources[currentIndex].seq = resource.seq-1;
        
    };
   
  });
