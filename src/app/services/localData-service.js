'use strict';
/**
 * @ngdoc service
 * @name ratadmin.localData
 * @description
 * # localData
 * Service in the ratadmin.
 */
angular.module('ratadmin')
  .factory('localDataService', function ($q,WorkType,localStorageService, Booking,$rootScope, User, SignupRequest, LeaveType, Leave, Holiday, $interval) {
  	var localDataService = {};
  	localDataService.workTypes = [];
  	localDataService.requestCounter = 0;
    localDataService.events = [];
    localDataService.selectedDate = moment().startOf('day').toISOString();
    localDataService.shiftMonths = [];
    var counter = moment();
    for(var i=0;i<6;i++) {
      var item = {
        name:counter.format('MMMM YY'),
        number: counter.format('M')
      };
      localDataService.shiftMonths.push(item);
      counter.add(1,'months');
    };
    counter.set({'hour':0,'minute':0});
    localDataService.slotLabels = [];
    for(var i=0;i<48;i++) {
      localDataService.slotLabels.push(counter.format('hh:mm a'));
      counter.add(30,'minutes');
    };

    localDataService.getHolidays = function() {
      if(localDataService.requestCounter>0) {
        localDataService.requestCounter--;
      }
      var defer = $q.defer();
      Holiday.find(function(success){
        localDataService.holidays = success;
        defer.resolve();
      }, function(error){
        console.error("Failed to get holidays",error);
        defer.reject();
      });
      return defer.promise;
    };

  	localDataService.getWorkTypes = function() {
  		if(localDataService.requestCounter>0) {
  			localDataService.requestCounter--;
  		}
  		var defer = $q.defer();
  		WorkType.find(function(result){
  		  if(result.$resolved) {
  		    localDataService.workTypes = result;
          console.log(localDataService.workTypes);
  		    defer.resolve();
  		  }
  		  else {
  		    console.error("Failed to get work types",result);
  		    defer.reject();
  		  }
  		});
  		return defer.promise;
  	};

    localDataService.getLeaveTypes = function() {
      if(localDataService.requestCounter>0) {
        localDataService.requestCounter--;
      }
      var defer = $q.defer();
      LeaveType.find(function(result){
        localDataService.leaveTypes = result;
        console.log(localDataService.leaveTypes);
        defer.resolve();
      },function(error){
        console.error("Failed to get leave types",error);
        defer.reject();
      });
      return defer.promise;
    };
    localDataService.getLeaves = function() {
      if(localDataService.requestCounter>0) {
        localDataService.requestCounter--;
      }
      var defer = $q.defer();
      // Leave.getLeaves(function(success){
      //   console.log(success);
      // },function(error){

      // });
      Leave.find(function(result){
        localDataService.leaves = result;
        _.each(localDataService.leaves,function(leave){
          leave.resourceName = getResourceName(leave.resourceID);
          leave.duration = getDuration(leave);
        });
        console.log(localDataService.leaves);
        defer.resolve();
      },function(error){
        console.error("Failed to get leave",error);
        defer.reject();
      });

      function getResourceName(id) {
        var result = _.find(localDataService.resources,function(item){return item.id==id});
        if(result) {
          return result.firstName+' '+result.lastName;
        }
        else {
          console.log(id);
          return "";
        }
      }
      function getDuration(leave) {
        return moment(leave.end,moment.ISO_8601).businessDiff(moment(leave.start,moment.ISO_8601),'day')+1;
      };

      return defer.promise;
    };

    localDataService.getCalendar = function(date,resourceID) {
      var defer = $q.defer();
      var user = localStorageService.get('user');
      if(!date) {
        date = localDataService.selectedDate;
      }
      Booking.getCalendarFor({"date":date,"resource":resourceID},function(response){
        if(response.$resolved) {
          var temp = localDataService.resources;
          localDataService.resources = null;
          localDataService.resources = temp;
          localDataService.currentCalendar = response.calendar;
          localDataService.calendarLabels = response.labels;
          defer.resolve();
        }
        else {
          defer.reject();
          console.log('Calendar find failed',response);
        }
      });
      return defer.promise;
    };

  	localDataService.getBookings = function() {
      var defer = $q.defer();
  		var user = localStorageService.get('user');
      if(user) {
        var today = moment().startOf('day').toDate();
        if(_.indexOf(user.roles,'resource')!=-1) {
          Booking.getUpcoming({"resourceID":user.id},function(result){
            localDataService.bookings = result.bookings;
            defer.resolve();
          }, function(error){
            console.log(error);
          });
        }
        else if(_.indexOf(user.roles,'employee')!=-1) {
          Booking.find({"filter":{"where":{"employeeID":user.id}}},function(result){
            localDataService.bookings = result;
            defer.resolve();
          }, function(error){
            console.log(error);
          });
        }
      }
      return defer.promise;
  	}
  	localDataService.getEmployees = function() {
  		var defer = $q.defer();
  		User.getUsers({type:"employee"},function(result){
  			if(localDataService.requestCounter>0) {
	  			localDataService.requestCounter--;
	  		}
  			if(result.$resolved) {
  				localDataService.employees = result.users;
  				console.log("Got employee list",localDataService.employees);
  				defer.resolve();
  			}
  			else {
  				console.error("Failed to get employees",result);
  				defer.reject();
  			}
  		}); 
  		return defer.promise;
  	};
  	localDataService.getResources = function() {
  		var defer = $q.defer();
  		User.getUsers({type:"resource"},function(result){
  			if(localDataService.requestCounter>0) {
	  			localDataService.requestCounter--;
	  		}
  			if(result.$resolved) {
  				localDataService.resources = result.users;
          var seq = 1;
          _.each(localDataService.resources,function(resource){
            resource.selected = true;
            if(resource.seq && resource.seq>seq) {
              seq = resource.seq;
            }
          });
          _.each(localDataService.resources,function(resource){
            if(!resource.seq) {
              resource.seq = seq;
              seq++;
            }
          });
  				console.log("Got resource list");
          localDataService.getLeaves();
  				defer.resolve();
  			}
  			else {
  				console.error("Failed to get resources",result);
  				defer.reject();
  			}
  		}); 
  		return defer.promise;
  	};
  	localDataService.getBaseSlots = function() {
  		var defer = $q.defer();
  		BaseSlot.find(function(result){
  			if(localDataService.requestCounter>0) {
	  			localDataService.requestCounter--;
	  		}
  			if(result.$resolved) {
  				localDataService.baseSlots = result;
  				console.log("Got base slot list");
  				defer.resolve();
  			}
  			else {
  				console.error("Failed to get employees",result);
  				defer.reject();
  			}
  		}); 
  		return defer.promise;
  	};
  	localDataService.getSignUpRequests = function() {
  		var defer = $q.defer();
  		SignupRequest.find(function(result){
  			if(localDataService.requestCounter>0) {
	  			localDataService.requestCounter--;
	  		}
  			if(result.$resolved) {
  				localDataService.signupRequests = result;
  				console.log("Got signup request list");
  				defer.resolve();
  			}
  			else {
  				console.error("Failed to get employees",result);
  				defer.reject();
  			}
  		}); 
  		return defer.promise;
  	};
  	localDataService.bootData = function() {
  		localDataService.requestCounter = 3;
  		localDataService.getWorkTypes();
  		localDataService.getEmployees();
  		localDataService.getBookings();
  		localDataService.getResources();
  		localDataService.getSignUpRequests();
      localDataService.getLeaveTypes();
  		localDataService.getHolidays();
      $interval(function(){
        localDataService.getCalendar();
      },120000);
  	};
  	return localDataService;
  });
