'use strict';
/**
 * @ngdoc service
 * @name ratadmin.localUser
 * @description
 * # localUser
 * Factory in the ratadmin.
 */
angular.module('ratadmin')
  .factory('localUserFactory', function (localStorageService,$state,$rootScope,localDataService,$q) {
// Service logic
// ...
    var localUser = {};
    if(localStorageService.get('user')) {
    	localUser.userData = localStorageService.get('user');
        localUser.roles = {};
        _.each(localUser.userData.roles,function(role){
            localUser.roles[role] = true;
        });
    	localDataService.bootData();
    }
    else {
    	$state.go('login');
    }
    localUser.setUser = function(user,token) {
    	localUser.userData = user;
        localUser.userData.token = token;
        if(_.indexOf(user.roles,'admin')!=-1) {
            localUser.userData.isAdmin = true;
        }
        localUser.roles = {};
        _.each(user.roles,function(role){
            localUser.roles[role] = true;
        });
        localStorageService.set('user',user);
        localDataService.bootData();
    };
    return localUser;
  });
