describe('Service: localDataService', function() {
  var localData;
  beforeEach(module('ratadmin'));
  beforeEach(inject(function(_localDataService_) {
  localData = _localDataService_;
  }));

  it('should attach a list of awesomeThings to the service', function() {
    expect(localData.awesomeThings.length).toBe(3);
  });

});
